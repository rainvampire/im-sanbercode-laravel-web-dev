<?php

    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    //release 0

    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name . "<br>"; // "shaun"
    echo "Legs : " . $kodok->legs . "<br>"; // 4
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; // "no"
    echo $kodok->jump() . "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->name . "<br>"; // "shaun"
    echo "Legs : " . $sungokong->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
    echo $sungokong->yell() . "<br><br>";


?>