<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', function () {
    return view('welcome');
});


Route::get('/table', function () {
    return view('page.table');
});

Route::get('/data-table', function () {
    return view('page.data-table');
});

// CRUD

// Create Data
// Route mengarah ke form tambah kategori

Route::get('/cast/create', [CastController::class, 'create']);

//Route untuk menyimpan ke table cast
Route::post('/cast', [CastController::class, 'store']);