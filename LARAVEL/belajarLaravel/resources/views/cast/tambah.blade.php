@extends('layouts.master')

@section('title')
    Halaman Tambah Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')

<form action="/cast" method="POST">
    <div class="form-group">
      <label>Cast Name</label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur Cast</label>
        <input type="integer" name="age" class="form-control">
      </div>
    @error('age')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Cast Bio</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection